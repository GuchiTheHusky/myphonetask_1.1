package MyPhone;

public class NokiaPhone extends Phone implements PhoneConnection {
    final String censorship = "motherf@cker";

    public NokiaPhone(String phoneName, String phoneModel, int memoryCapacity, int memoryRAM) {
        super(phoneName, phoneModel, memoryCapacity, memoryRAM);
    }


    @Override
    public String getCallMeBack() {
        return "I'm not your girlfriend, don't send me anything " + censorship;
    }

    @Override
    public String getMessageToSomeGirlfriend() {
        return null;
    }


    // Добавив новий метод.
    public String getPhoneFullInfo() {
        return getPhoneName() + " " + getPhoneModel() + " " + getMemoryCapacity() + " MB " + getMemoryRAM() + " MB";
    }
}

