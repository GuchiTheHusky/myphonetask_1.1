package MyPhone;

public class Main {

    public static void main(String[] args) throws InterruptedException {

        NokiaPhone nokia = new NokiaPhone("Nokia", "3300", 512, 64);
        SamsungPhone samsung = new SamsungPhone("Samsung", "A51", 7956, 1500);
        Thread.sleep(1000);
        System.out.println("Some girl phone info: " + "\n" + "<<<NOKIA>>>" + "\n" + nokia.getPhoneFullInfo());
        System.out.println();
        Thread.sleep(1000);
        System.out.println("My phone info: " + "\n" + "<<<SAMSUNG>>>" + "\n" + samsung.getPhoneFullInfo());
        System.out.println();
        Thread.sleep(1000);
        System.out.println("Photo of Everest: ");
        System.out.println();
        Thread.sleep(1000);
        System.out.println(samsung.getPhotoOfEverest());
        System.out.println();
        Thread.sleep(1000);
        System.out.println("Send sms to some girlfriend:" + "\n" + samsung.getMessageToSomeGirlfriend());
        Thread.sleep(1000);
        System.out.println("She call me back, and say: ");
        Thread.sleep(1000);
        System.out.println(nokia.getCallMeBack() + ".");
        Thread.sleep(1000);
        System.out.println("The End.");
    }

}
